import java.io.File;
import java.io.IOException;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

/**
 * 
 * 
 * API & Resource
 * https://kb.itextpdf.com/home/it7kb/examples
 * @author lukelyu
 *
 */
public class PDF_SAMPLE_01 {
	
	public static final String DEST = "results/PDF_SAMPLE_01.pdf";

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PDF_SAMPLE_01().createPdf(DEST);
	}
	
	public void createPdf(String dest) throws IOException {
        
		//Initialize PDF writer
        PdfWriter writer = new PdfWriter(dest);
        
        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);
        
        // Initialize document
        Document document = new Document(pdf);
        
        //Add paragraph to the document
        document.add(new Paragraph("SAMPLE_PDF_01_MAKE_PDF_FILE!"));
        
        //Close document
        document.close();
        
        //FINISH
        System.out.println("SAMPLE_PDF_01_MAKE_PDF_FILE is FINISH...");
    }

}
