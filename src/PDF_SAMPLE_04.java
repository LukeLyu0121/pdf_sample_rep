import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.UnitValue;

public class PDF_SAMPLE_04 {
	
	public static final String DATA = "united_states.csv";
    public static final String DEST = "results/PDF_SAMPLE_04_TABLE.pdf";
    
    public static void main(String args[]) throws IOException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PDF_SAMPLE_04().createPdf(DEST);
    }
    
    public void createPdf(String dest) throws IOException {
    	//Initialize PDF writer
        PdfWriter writer = new PdfWriter(dest);

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf, PageSize.A4.rotate());
        document.setMargins(20, 20, 20, 20);//上下左右邊界

        PdfFont font = PdfFontFactory.createFont(StandardFonts.HELVETICA);//字型
        PdfFont bold = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLD);//字體粗體 (title使用)
        
        Table table = new Table(UnitValue.createPercentArray(new float[]{4, 1, 3, 4, 3, 3, 3, 3, 1}))
                .useAllAvailableWidth();//依照陣列比例設定table個欄寬
        BufferedReader br = new BufferedReader(new FileReader(DATA));//讀取檔案內容
        String line = br.readLine();//每行讀取
        process(table, line, bold, true);//加入表頭標題
        while ((line = br.readLine()) != null) {//加入表格內容
            process(table, line, font, false);
        }
        br.close();
        document.add(table);

        //Close document
        document.close();
    	
    	
    	//FINISH
        System.out.println("PDF_SAMPLE_04_TABLE is FINISH...");
    }
    
    public void process(Table table, String line, PdfFont font, boolean isHeader) {
        StringTokenizer tokenizer = new StringTokenizer(line, ";");
        while (tokenizer.hasMoreTokens()) {
            if (isHeader) {
                table.addHeaderCell(new Cell().add(new Paragraph(tokenizer.nextToken()).setFont(font)));
            } else {
                table.addCell(new Cell().add(new Paragraph(tokenizer.nextToken()).setFont(font)));
            }
        }
    }
    
}
