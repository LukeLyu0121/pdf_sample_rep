import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.barcodes.qrcode.EncodeHintType;
import com.itextpdf.barcodes.qrcode.ErrorCorrectionLevel;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.List;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PDF_SAMPLE_ITEM_INFO {
	
    public static final String DEST = "./results/PDF_SAMPLE_ITEM_INFO.pdf";
    public static final String ITEM_IMG = "item.png";
    

    public static void main(String[] args) throws Exception {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PDF_SAMPLE_ITEM_INFO().createPdf(DEST);
    }
    
    
    public void createPdf(String dest) throws IOException {
    	 
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
        Document doc = new Document(pdfDoc);
     
        String code = "675-FH-A12";
        Barcode128 code128 = new Barcode128(pdfDoc);
        code128.setFont(null);
        code128.setCode(code);
        code128.setCodeType(Barcode128.CODE128);
     
        Table table = new Table(2);
        
        Image qrcode = new Image(createBarQrCode("ITEM_CODE_INFO").createFormXObject(pdfDoc)).setAutoScale(true);
        Paragraph para = new Paragraph("QRcode - " + code).setTextAlignment(TextAlignment.CENTER);
        
        Cell cell = new Cell();
        cell.add(qrcode);
        cell.add(para);
        table.addCell(cell);
     
        Image code128Image = new Image(code128.createFormXObject(pdfDoc)).setAutoScale(true);
        PdfFont font = PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN);
        
        //add list
        List list = new List()
            .setSymbolIndent(12)
            .setListSymbol("\u2022")
            .setFont(font);
        
        ListItem item = new ListItem("ITEM_ACTUAL_PRICE ");
		item.setFont(font).setFontColor(ColorConstants.RED);
		ListItem itemPrice = new ListItem("ITEM_PROMOTION_PRICE ");
		itemPrice.setFont(font).setFontColor(ColorConstants.RED);
        
        // Add ListItem objects
        list.add(new ListItem("ITEM_NAME . (ITEM_NAME)"))
            .add(new ListItem("ITEM_C_NAME (ITEM_C_NAME) ITEM_E_NAME (ITEM_E_NAME)."))
            .add(new ListItem("ITEM_DESCRIPTION_TITLE (ITEM_DESCRIPTION_TITLE)."))
            .add(item).add(itemPrice);
        Paragraph paragraph = new Paragraph("Barcode - " + code).add(list).setTextAlignment(TextAlignment.CENTER);
     
        Cell cell2 = new Cell();
        cell2.add(code128Image);
        cell2.add(paragraph);
        table.addCell(cell2);
     
        doc.add(table);
        doc.close();
    }

    public BarcodeQRCode createBarQrCode(String content) {
    	
        StringBuffer sb = new StringBuffer ();
        sb.append ( content );

        Map<EncodeHintType, Object> qrParam = new HashMap<EncodeHintType, Object> ();
        qrParam.put ( EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M );
        qrParam.put ( EncodeHintType.CHARACTER_SET, "UTF-8" );

        BarcodeQRCode qrcode = new BarcodeQRCode ( sb.toString (), qrParam );
        return qrcode;
    	
    }
    
}                                                   

